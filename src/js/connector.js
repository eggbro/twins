var GRAY_ICON = 'https://cdn.hyperdev.com/us-east-1%3A3d31b21c-01a0-4da2-8827-4bc6e88b7618%2Ficon-gray.svg';

const onRemoveTwinBtnClick = (t, opts)  => {

  t.set('card','shared',{twinId: null})

  console.log('Someone clicked removeTwin button');
};

const onAddTeinBtnClick = (t,opts) => {
  t.set('card','shared',{twinId: "NewTwinId"})

  console.log('Someone clicked addTwin button');
}

const popUp = {
  icon: GRAY_ICON, // don't use a colored icon here
        text: 'Popup',
        callback: (t,opts) => {
          return t.popup({
          title: "PopUp Wup",
          url: 'popup.html'
        });
        },
        condition: 'edit'
}

const showPopUp = (t) => {
  return t.popup({
  title: "PopUp Wup",
  url: 'popup.html'
});
}

const twinMain = (t, opts) => {
  t.getRestApi()
  .isAuthorized()
  .then((isAuth) => console.log("Authorized", isAuth))

  return t.get('card','shared')
  .then((data) => {
    console.log(data)
    if(data && data.twinId){
      return [{
        icon: GRAY_ICON, // don't use a colored icon here
        text: 'Remove twin',
        callback: onRemoveTwinBtnClick,
        condition: 'edit'
      }, popUp];
    }
    else{
      return [{
        icon: GRAY_ICON, // don't use a colored icon here
        text: 'Add twin',
        callback: onAddTeinBtnClick,
        condition: 'edit'
      }, popUp];
    }
  })
}

window.TrelloPowerUp.initialize({
  'card-buttons': twinMain,
'authorization-status': function(t, options){
  return t.get('member', 'private', 'authToken')
  .then(function(authToken) {
    return { authorized: authToken != null }
  });
},

'show-authorization': function(t, options){
  return t.popup({
    title: 'Authorize 🥑 Account',
    url: './auth.html',
    height: 140,
  });
}
},
{
  appKey: '%%TRELLO_APP_KEY%%',
  appName: 'Twins Power-Up'
});