const t = TrelloPowerUp.iframe({
    appKey: '%%TRELLO_APP_KEY%%',
    appName: 'Twins Power-Up'
  });

  let client  = t.getRestApi()
  client
  .isAuthorized()
  .then((isAuth) => console.log("Authorized iFrame", isAuth))

  console.log(client)